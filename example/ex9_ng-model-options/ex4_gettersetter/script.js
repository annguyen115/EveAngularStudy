angular.module('myapp', [])
.controller('optionsController', ['$scope', function($scope) {
    var _name = 'Freetuts.net';
    $scope.user = {
        name: function(newName) {
                return angular.isDefined(newName) ? (_name = newName) : _name;
            }
        };
    }]);