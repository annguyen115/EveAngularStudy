angular.module('MyForm', [])
.controller('ExampleController', ['$scope', function($scope) 
{
    // Khởi tạo giá trị ban đầu
    $scope.form = {
        title : 'Trò Chơi Tính Toán',
        num1 : 'Số thứ nhất',
        num2: 'Số thứ hai',
    };
    // vì ban đầu chưa nhập gì nên ẩn khung kết quả
    $scope.styleresult = 'display:none';
    // Khi nhập các số vào các input thì gọi sự kiện này
    $scope.show_result = function(){
        // Nếu validate form đúng
        if ($scope.calForm.$valid){
                
                // $scope.styleresult = 'display:block';
                // lấy data từ form
                let a = parseInt($scope.so_thu_nhat);
                let b = parseInt($scope.so_thu_hai);
                $scope.message = {
                        phep_cong : "Cộng hai số:",
                        phep_tru : "Trừ hai số:",
                        phep_nhan : "Nhân hai số:",
                        phep_chia : "Chia hai số:"
                };
                $scope.message.phep_cong += a+b; 
                $scope.message.phep_tru  += a-b;
                $scope.message.phep_nhan += a*b;
                $scope.message.phep_chia += a/b;
        }
        // nếu validate form sai thì ẩn result
        else {
                //ẩn giả trị mới tính
                $scope.message = {};
                // $scope.styleresult = 'display:none';
        }
    }    
}]);